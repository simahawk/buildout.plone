#!/bin/sh

# fixes this issue https://github.com/buildout/buildout/issues/63
# run it whenever bin/buildout complains about bad syntax into .installed.cfg

sed -i 's/+ =/+=/g' .installed.cfg
