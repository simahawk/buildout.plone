Old-style Zope products you are developing can be added here.
Don't use this directory for production environment.
For products not managed through buildout.cfg, use src/ .
