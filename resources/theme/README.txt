
########################################################################################
####                          STATIC THEME                                          ####
########################################################################################

Place your static theme resources in resources/theme.

You theme must include a `manifest.cfg` file like this:

[theme]
title = miotema.theme
description = Mio Theme
rules = /++theme++miotema.theme/theme/rules/rules.xml
doctype = <!DOCTYPE html>

Check this document for further reference:

http://projects.abstract.it/documentation/procedure/in-sperimentazione/tema-statico

########################################################################################
########################################################################################


